-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"

--------------------------------------------

-- forward declarations and other locals
local SCREEN_WIDTH, SCREEN_HEIGHT = display.actualContentWidth, display.actualContentHeight
local HALF_WIDTH, HALF_HEIGHT = display.contentCenterX, display.contentCenterY

local sceneGroup

local thumbCircle
local holdingThumbCircle = false
local thumbTouchX, thumbTouchY
local THUMB_CIRCLE_RADIUS = HALF_WIDTH / 10
local THUMB_CIRCLE_SPEED = 25
local thumbCircleCollision = true

local boxes = {}
local SCREEN_WIDTH_IN_BOXES = 5
local DEFAULT_BOX_DIMENSIONS = SCREEN_WIDTH / SCREEN_WIDTH_IN_BOXES
local previousPattern = 0
local currentBoxSpeed = 1250.0
local MAX_BOX_SPEED = 3000.0
local boxAcceleration = 14
local BOX_DENSITY = 0.1

local previousUpdateTime = 0
local currentUpdateTime

distanceTravelled = 0
achievedHighScore = false
local distanceAtNextPatternSpawn = 3000

local scoreText
local alive = true

local powerups = {}
local SCREEN_WIDTH_IN_POWERUPS = 6
local DEFAULT_POWERUP_DIMENSIONS = SCREEN_WIDTH / SCREEN_WIDTH_IN_POWERUPS
local POWERUP_DENSITY = BOX_DENSITY * (math.pow(DEFAULT_BOX_DIMENSIONS, 2)) / (math.pi * math.pow(DEFAULT_POWERUP_DIMENSIONS / 2, 2))
extraPoints = 0
local invinciblePowerupCounter = 0

function gameLoop()
	if alive then
		thumbCircle.angularVelocity = 0
		thumbCircle.rotation = 0
		
		if (holdingThumbCircle) then
			thumbCircle:setLinearVelocity((thumbTouchX - thumbCircle.x) * THUMB_CIRCLE_SPEED, (thumbTouchY - thumbCircle.y) * THUMB_CIRCLE_SPEED)
		else
			thumbCircle:setLinearVelocity(0, 0)
		end
		
		local vx, vy = thumbCircle:getLinearVelocity()
		
		if (thumbCircle.x - THUMB_CIRCLE_RADIUS <= 0 and vx <= 0) then
			thumbCircle:setLinearVelocity(0, 0)
			thumbCircle.x = THUMB_CIRCLE_RADIUS
		end
		if (thumbCircle.x + THUMB_CIRCLE_RADIUS >= SCREEN_WIDTH and vx >= 0) then
			thumbCircle:setLinearVelocity(0, 0)
			thumbCircle.x = SCREEN_WIDTH - THUMB_CIRCLE_RADIUS
		end
		if (thumbCircle.y - THUMB_CIRCLE_RADIUS <= 0 and vy <= 0) then
			thumbCircle:setLinearVelocity(0, 0)
			thumbCircle.y = THUMB_CIRCLE_RADIUS
		end
		if (thumbCircle.y + THUMB_CIRCLE_RADIUS >= SCREEN_HEIGHT and vy >= 0) then
			thumbCircle:setLinearVelocity(0, 0)
			thumbCircle.y = SCREEN_HEIGHT - THUMB_CIRCLE_RADIUS
		end
		
		for i=#boxes, 1, -1 do
			box = boxes[i]
			if (box.y - box.height / 2 > SCREEN_HEIGHT) then
				box:removeSelf()
				table.remove(boxes, i)
			end
		end
		
		for i=#powerups, 1, -1 do
			powerup = powerups[i]
			if (powerup.y - powerup.path.radius > SCREEN_HEIGHT) then
				powerup:removeSelf()
				table.remove(powerups, i)
			end
		end
		
		currentUpdateTime = os.clock()
		local deltaTime = currentUpdateTime - previousUpdateTime
		
		distanceTravelled = distanceTravelled + deltaTime * currentBoxSpeed
		scoreText.text = math.floor(distanceTravelled + extraPoints)
		
		if currentBoxSpeed ~= MAX_BOX_SPEED then
			currentBoxSpeed = currentBoxSpeed + (deltaTime) * boxAcceleration
			if currentBoxSpeed > MAX_BOX_SPEED then
				currentBoxSpeed = MAX_BOX_SPEED
			end
		end
		
		for index, box in ipairs(boxes) do
			box:setLinearVelocity(0, currentBoxSpeed)
		end
		
		for index, powerup in ipairs(powerups) do
			powerup:setLinearVelocity(0, currentBoxSpeed)
		end
		
		if distanceTravelled >= distanceAtNextPatternSpawn then
			spawnPattern()
		end
		
		previousUpdateTime = currentUpdateTime
	end
end

function scene:create( event )
	sceneGroup = self.view

	-- We need physics started to add bodies, but we don't want the simulaton
	-- running until the scene is on the screen.
	physics.start()
	physics.pause()
	physics.setGravity(0, 0)
	physics.setScale(15 * (SCREEN_HEIGHT / 1080.0))
	
	thumbCircle = display.newCircle(HALF_WIDTH, HALF_HEIGHT * 1.5, THUMB_CIRCLE_RADIUS)
	physics.addBody(thumbCircle, "dynamic", {density = 10, friction = 0.0, bounce = 0.0})
	thumbCircle.gravityScale = 0
	thumbCircle.isSensor = true
	thumbCircle:setFillColor(1)
	thumbCircle.strokeWidth = 4
	thumbCircle:setStrokeColor(0)
	
	local function touchThumbCircle(event)
		if alive then
			if (event.phase == "began") then
				holdingThumbCircle = true
				thumbCircle:setFillColor(0.8, 0, 0)
				thumbTouchX = event.x
				thumbTouchY = event.y
			end
			return true
		end
	end
	thumbCircle:addEventListener("touch", touchThumbCircle)
	
	local function screenRelease(event)
		if alive then
			if (event.phase == "ended") then
				holdingThumbCircle = false
				thumbCircle:setFillColor(1)
			elseif (event.phase == "moved" and holdingThumbCircle) then
				thumbTouchX = event.x
				thumbTouchY = event.y
			end
		end
	end
	local screenButton = display.newRect(HALF_WIDTH, HALF_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)
	screenButton:setFillColor(0, 0, 0, 0.01)
	screenButton:addEventListener("touch", screenRelease)
	
	scoreText = display.newText("0", SCREEN_WIDTH * 0.8, SCREEN_HEIGHT * 0.1, "res/troika.otf", 50)
	
	--[[leftRect = display.newRect(-DEFAULT_BOX_DIMENSIONS / 2, HALF_HEIGHT, DEFAULT_BOX_DIMENSIONS, SCREEN_HEIGHT)
	physics.addBody(leftRect, "kinematic")
	rightRect = display.newRect(SCREEN_WIDTH + DEFAULT_BOX_DIMENSIONS / 2, HALF_HEIGHT, DEFAULT_BOX_DIMENSIONS, SCREEN_HEIGHT)
	physics.addBody(rightRect, "kinematic")]]--

	-- create a grey rectangle as the backdrop
	-- the physical screen will likely be a different shape than our defined content area
	-- since we are going to position the background from it's top, left corner, draw the
	-- background at the real top, left corner.
	local background = display.newRect( display.screenOriginX, display.screenOriginY, SCREEN_WIDTH, SCREEN_HEIGHT )
	background.anchorX = 0 
	background.anchorY = 0
	background:setFillColor(0.85)
	
	sceneGroup:insert(background)
	sceneGroup:insert(thumbCircle)
	sceneGroup:insert(scoreText)
	
	previousUpdateTime = os.clock()
	gameLoopTimer = timer.performWithDelay(16, gameLoop, 0)
end

function spawnPattern()
	local type = math.random(0, 15)
	if type == previousPattern then
		if type == 0 then
			type = 4
		else
			type = type - 1
		end
	end
	
	previousPattern = type
	
	print(type)
	
	if type == 0 then
		spawnRandomPattern()
		distanceAtNextPatternSpawn = distanceTravelled + 3500
		-- spawnXPattern()
		-- distanceAtNextPatternSpawn = distanceTravelled + 1500
		-- gameLoopTimer = timer.performWithDelay(3000, spawnPattern, 1)
	elseif type == 1 then
		spawnStairCase()
		distanceAtNextPatternSpawn = distanceTravelled + 1750
		-- gameLoopTimer = timer.performWithDelay(3000, spawnPattern, 1)
	elseif type == 2 then
		spawnDualPattern()
		distanceAtNextPatternSpawn = distanceTravelled + 3000
		-- gameLoopTimer = timer.performWithDelay(5000, spawnPattern, 1)
	elseif type == 3 then
		spawnAlternatingPattern()
		distanceAtNextPatternSpawn = distanceTravelled + 4000
		-- gameLoopTimer = timer.performWithDelay(5000, spawnPattern, 1)
	elseif type == 4 then
		spawnPlatformPattern()
		distanceAtNextPatternSpawn = distanceTravelled + 6500
		-- gameLoopTimer = timer.performWithDelay(7000, spawnPattern, 1)
	elseif type == 5 then
		spawnAlternatingStaircases()
		distanceAtNextPatternSpawn = distanceTravelled + 10000
	elseif type == 6 then
		spawnSquareWavePattern()
		distanceAtNextPatternSpawn = distanceTravelled + 6500
	elseif type == 7 then
		spawnSawToothWavePattern()
		distanceAtNextPatternSpawn = distanceTravelled + 12750
	elseif type == 8 then
		spawnSwitchingBlocks()
		distanceAtNextPatternSpawn = distanceTravelled + 5500
	elseif type == 9 then
		spawnSineWavePattern()
		distanceAtNextPatternSpawn = distanceTravelled + 16300
	else
		-- By default, do this
		spawnRandomPattern()
		distanceAtNextPatternSpawn = distanceTravelled + 3500
	end
end

function die()
	alive = false
	currentBoxSpeed = 0
	thumbCircle:setFillColor(0.8, 0, 0.8)
	thumbCircle:setLinearVelocity(0, 0)
	
	for index, box in ipairs(boxes) do
		box:setLinearVelocity(0, 0)
	end
	
	for index, powerup in ipairs(powerups) do
		powerup:setLinearVelocity(0, 0)
	end
	
	if saveData and saveData.highScore < distanceTravelled then
		achievedHighScore = true
		saveData2 = saveData
		saveData2["highScore"] = math.floor(distanceTravelled + extraPoints)
		jsonIO.saveTable(saveData2, "save_data.json")
	elseif not saveData then
		achievedHighScore = true
		local savaData2 = {
			highScore = math.floor(distanceTravelled)
		}
		print("NOPE")
		jsonIO.saveTable(savaData2, "save_data.json")
	end
	
	function gotoGameOver()
		for index, box in ipairs(boxes) do
			sceneGroup:insert(box)
		end
		for index, powerup in ipairs(powerups) do
			sceneGroup:insert(powerup)
		end
		composer.gotoScene("game_over", "fade", 500)
	end
	
	timer.performWithDelay(1000, gotoGameOver, 1)
end

function spawnTestBox()
	spawnBox(HALF_WIDTH, -DEFAULT_BOX_DIMENSIONS / 2)
end

function spawnBox(x, y)
	local box = display.newRect(x, y, DEFAULT_BOX_DIMENSIONS, DEFAULT_BOX_DIMENSIONS)
	physics.addBody(box, "kinematic", {density = BOX_DENSITY, friction = 0.1, bounce = 0.0})
	box.strokeWidth = 4
	box.isSensor = true
	box:setStrokeColor(0)
	
	local function onLocalCollision(self, event)
		-- Temporary checking
		if (event.other == thumbCircle and thumbCircleCollision) then
			die()
		end
	end
	
	box.collision = onLocalCollision
	box:addEventListener("collision")
	table.insert(boxes, box)
end

function spawnXPattern()
	spawnBox(DEFAULT_BOX_DIMENSIONS * 0.5, -DEFAULT_BOX_DIMENSIONS / 2)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 0.5, -DEFAULT_BOX_DIMENSIONS * 5.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 1.5, -DEFAULT_BOX_DIMENSIONS * 1.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 1.5, -DEFAULT_BOX_DIMENSIONS * 4.5)
	--[[spawnBox(DEFAULT_BOX_DIMENSIONS * 3.5, -DEFAULT_BOX_DIMENSIONS * 2.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 3.5, -DEFAULT_BOX_DIMENSIONS * 5.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 6.5, -DEFAULT_BOX_DIMENSIONS * 2.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 6.5, -DEFAULT_BOX_DIMENSIONS * 5.5)]]--
	spawnBox(DEFAULT_BOX_DIMENSIONS * 4.5, -DEFAULT_BOX_DIMENSIONS * 1.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 4.5, -DEFAULT_BOX_DIMENSIONS * 4.5)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 5.5, -DEFAULT_BOX_DIMENSIONS / 2)
	spawnBox(DEFAULT_BOX_DIMENSIONS * 5.5, -DEFAULT_BOX_DIMENSIONS * 5.5)
end

function spawnRandomPattern()
	local previous = 0
	for i = 0, SCREEN_WIDTH_IN_BOXES * 2, 1 do
		local x = math.random(0, SCREEN_WIDTH_IN_BOXES - 1)
		if x == 0 and previous == 1 then
			x = 1
		elseif x == SCREEN_WIDTH_IN_BOXES - 1 and previous == SCREEN_WIDTH_IN_BOXES - 2 then
			x = SCREEN_WIDTH_IN_BOXES - 2
		end
		previous = x
		spawnBox(DEFAULT_BOX_DIMENSIONS * (x + 0.5), -DEFAULT_BOX_DIMENSIONS * i - 0.5)
	end
end

function spawnDualPattern()
	local previous = 0
	for i = 0, 4, 1 do
		local r = math.random(0, SCREEN_WIDTH_IN_BOXES - 1)
		if r == previous then
			if r ~= SCREEN_WIDTH_IN_BOXES - 1 then
				r = r + 1
			else
				r = r - 1
			end
		end
		
		if r == SCREEN_WIDTH_IN_BOXES - 1 and previous == SCREEN_WIDTH_IN_BOXES - 2 then
			r = SCREEN_WIDTH_IN_BOXES - 3
		elseif r == 0 and previous == 1 then
			r = 2
		end
		
		previous = r
		
		local x = DEFAULT_BOX_DIMENSIONS * (r + 0.5)
		spawnBox(x, -DEFAULT_BOX_DIMENSIONS * (i * 2 + 0.5))
		spawnBox(x, -DEFAULT_BOX_DIMENSIONS * (i * 2 + 1.5))
		
		local r = math.random(6)
		if r == 1 then
			spawnPointPowerUp(DEFAULT_BOX_DIMENSIONS * (x + 1), -DEFAULT_BOX_DIMENSIONS * (i * 2 + 1.5))
		end
	end
end

function spawnSineWavePattern()
	for i = 0, 4, 1 do
		for j = 0, SCREEN_WIDTH_IN_BOXES - 1, 1 do
			for k = 0, SCREEN_WIDTH_IN_BOXES - 2 - j, 1 do
				spawnBox((k + 0.5) * DEFAULT_BOX_DIMENSIONS, -DEFAULT_BOX_DIMENSIONS * (j + 0.5) - DEFAULT_BOX_DIMENSIONS * i * SCREEN_WIDTH_IN_BOXES * 3)
			end
			for k = SCREEN_WIDTH_IN_BOXES - 1, SCREEN_WIDTH_IN_BOXES - j, -1 do
				spawnBox((k + 0.5) * DEFAULT_BOX_DIMENSIONS, -DEFAULT_BOX_DIMENSIONS * (j + 3.0) - DEFAULT_BOX_DIMENSIONS * i * SCREEN_WIDTH_IN_BOXES * 3)
			end
		end
		local r = math.random(4)
		if r == 1 then
			spawnPointPowerUp(DEFAULT_BOX_DIMENSIONS * 0.5, - DEFAULT_BOX_DIMENSIONS * i * SCREEN_WIDTH_IN_BOXES * 3 - DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES * 1.5))
		end
		for j = 0, SCREEN_WIDTH_IN_BOXES - 1, 1 do
			for k = 0, SCREEN_WIDTH_IN_BOXES - 2 - j, 1 do
				spawnBox((SCREEN_WIDTH_IN_BOXES - k - 1 + 0.5) * DEFAULT_BOX_DIMENSIONS, -DEFAULT_BOX_DIMENSIONS * (j + 0.5) - DEFAULT_BOX_DIMENSIONS * i * SCREEN_WIDTH_IN_BOXES * 3 - DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES * 1.5))
			end
			for k = SCREEN_WIDTH_IN_BOXES - 1, SCREEN_WIDTH_IN_BOXES - j, -1 do
				spawnBox((SCREEN_WIDTH_IN_BOXES - k - 1 + 0.5) * DEFAULT_BOX_DIMENSIONS, -DEFAULT_BOX_DIMENSIONS * (j + (SCREEN_WIDTH_IN_BOXES * 0.6)) - DEFAULT_BOX_DIMENSIONS * i * SCREEN_WIDTH_IN_BOXES * 3 - DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES * 1.5))
			end
		end
	end
end

function spawnAlternatingPattern()
	if SCREEN_WIDTH_IN_BOXES % 2 == 0 then
		-- If the # of boxes across the screen is even, try this pattern.
		for i = 0, 4, 1 do
			local r = math.random(6)
			if r == 1 then
				spawnInvinciblePowerUp(DEFAULT_BOX_DIMENSIONS * 0.5, -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			elseif r == 2 then
				spawnPointPowerUp(DEFAULT_BOX_DIMENSIONS * 0.5, -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			end
			spawnBox(DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES / 2 + ((i % 2) * -2) + 1.5), -DEFAULT_BOX_DIMENSIONS * (i * 2 + 0.5))
			-- spawnBox(DEFAULT_BOX_DIMENSIONS * (2.5 + (i % 2) * (SCREEN_WIDTH_IN_BOXES - 5)), -DEFAULT_BOX_DIMENSIONS * (i * 2 + 0.5))
		end
	else
		-- Otherwise, try this pattern!
		for i = 0, 4, 1 do
			local r = math.random(6)
			if r == 1 then
				spawnInvinciblePowerUp(DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			elseif r == 2 then
				spawnPointPowerUp(DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			end
			spawnBox(DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES / 2 + ((i % 2) * -2) + 1), -DEFAULT_BOX_DIMENSIONS * (i * 2 + 0.5))
		end
	end
end

function spawnPlatformPattern()
	local previous = 0
	for i = 0, 6, 1 do
		local hole = math.random(0, SCREEN_WIDTH_IN_BOXES - 1)
		
		if hole == previous then
			if hole ~= SCREEN_WIDTH_IN_BOXES - 1 then
				hole = hole + 1
			else
				hole = hole - 1
			end
		end
		
		previous = hole
		
		for j = 0, SCREEN_WIDTH_IN_BOXES - 1, 1 do
			if j ~= hole then
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 4 + 0.5))
			end
		end
	end
end

function spawnSquareWavePattern()
	local side = math.random(0, 1)

	for i = 0, 4, 1 do
		if (i + side) % 2 == 0 then
			for j = 0, SCREEN_WIDTH_IN_BOXES - 2, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 0.5))
			end
			
			spawnBox(DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 1.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			
			local r = math.random(5)
			if r == 1 then
				spawnInvinciblePowerUp(DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			end
			
			for j = 0, SCREEN_WIDTH_IN_BOXES - 2, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 2.5))
			end
		else
			for j = SCREEN_WIDTH_IN_BOXES - 1, 1, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 0.5))
			end
			
			spawnBox(DEFAULT_BOX_DIMENSIONS * 1.5, -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			
			local r = math.random(5)
			if r == 1 then
				spawnPointPowerUp(DEFAULT_BOX_DIMENSIONS / 2, -DEFAULT_BOX_DIMENSIONS * (i * 6 + 1.5))
			end
			
			for j = SCREEN_WIDTH_IN_BOXES - 1, 1, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * 6 + 2.5))
			end
		end
	end
end

function spawnSawToothWavePattern()
	local side = math.random(2)
	
	if side == 1 then
		for i = 0, 3, 1 do
			for j = 0, SCREEN_WIDTH_IN_BOXES - 2, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + j + 0.5))
			end
			
			for j = 0, SCREEN_WIDTH_IN_BOXES - 3, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + (SCREEN_WIDTH_IN_BOXES - 2) + 0.5))
			end
			
			for j = 1, SCREEN_WIDTH_IN_BOXES - 1, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (j + 3.5) - DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + (SCREEN_WIDTH_IN_BOXES - 2) + 0.5))
			end
			
			for j = 1, SCREEN_WIDTH_IN_BOXES - 1, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (1 + 3.5) - DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + (SCREEN_WIDTH_IN_BOXES - 2) + 0.5))
			end
		end
	else
		for i = 0, 3, 1 do
			for j = SCREEN_WIDTH_IN_BOXES - 1, 1, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + SCREEN_WIDTH_IN_BOXES - 1 - j + 0.5))
			end
			
			for j = SCREEN_WIDTH_IN_BOXES - 1, 1, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + (SCREEN_WIDTH_IN_BOXES - 2) + 0.5))
			end
			
			for j = SCREEN_WIDTH_IN_BOXES - 2, 0, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 2 + 3.5) - DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + SCREEN_WIDTH_IN_BOXES - 1 - j + 0.5))
			end
			
			for j = SCREEN_WIDTH_IN_BOXES - 2, 0, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (1 + 3.5) - DEFAULT_BOX_DIMENSIONS * (i * (SCREEN_WIDTH_IN_BOXES + 2) * 2 + (SCREEN_WIDTH_IN_BOXES - 2) + 0.5))
			end
		end
	end
end

function spawnStairCase()
	local side = math.random(2)
	
	if side == 1 then
		for i = 0, SCREEN_WIDTH_IN_BOXES - 2, 1 do
			spawnBox(DEFAULT_BOX_DIMENSIONS * (i + 0.5), -DEFAULT_BOX_DIMENSIONS * (i + 0.5))
		end
	else
		for i = SCREEN_WIDTH_IN_BOXES - 1, 1, -1 do
			spawnBox(DEFAULT_BOX_DIMENSIONS * (i + 0.5), -DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 1 - i + 0.5))
		end
	end
end

function spawnAlternatingStaircases()
	for stairCase = 0, 4, 1 do
		if stairCase % 2 == 1 then
			for i = 0, SCREEN_WIDTH_IN_BOXES - 2, 1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (i + 0.5), -DEFAULT_BOX_DIMENSIONS * (i + 0.5) - ((stairCase + 1) * DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES + 1)))
			end
		else
			for i = SCREEN_WIDTH_IN_BOXES - 1, 1, -1 do
				spawnBox(DEFAULT_BOX_DIMENSIONS * (i + 0.5), -DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES - 1 - i + 0.5) - ((stairCase + 1) * DEFAULT_BOX_DIMENSIONS * (SCREEN_WIDTH_IN_BOXES + 1)))
			end
		end
	end
end

function spawnSwitchingBlocks()
	for i = 0, 8, 1 do
		for j = 0, SCREEN_WIDTH_IN_BOXES, 1 do
			if i % 2 == j % 2 then
				spawnBox(DEFAULT_BOX_DIMENSIONS * (j + 0.5), -DEFAULT_BOX_DIMENSIONS * (i + 0.5) * 4)
			end
		end
	end
end

function spawnPointPowerUp(x, y)
	local powerup = display.newCircle(x, y, DEFAULT_POWERUP_DIMENSIONS / 2)
	physics.addBody(powerup, "kinematic", {density = POWERUP_DENSITY, friction = 0.1, bounce = 0.0})
	powerup.strokeWidth = 4
	powerup.isSensor = true
	powerup:setFillColor(0.8, 0.3, 0.8)
	powerup:setStrokeColor(0)
	
	local function onLocalCollision(self, event)
		if (event.other == thumbCircle) then
			extraPoints = extraPoints + 2000
			event.target:removeSelf()
			for i,v in pairs(powerups) do
				if v == event.target then
					table.remove(powerups, i)
				end
			end
		end
	end
	
	powerup.collision = onLocalCollision
	powerup:addEventListener("collision")
	table.insert(powerups, powerup)
end

function spawnInvinciblePowerUp(x, y)
	if thumbCircleCollision then
		local powerup = display.newCircle(x, y, DEFAULT_POWERUP_DIMENSIONS / 2)
		physics.addBody(powerup, "kinematic", {density = POWERUP_DENSITY, friction = 0.1, bounce = 0.0})
		powerup.strokeWidth = 4
		powerup.isSensor = true
		powerup:setFillColor(0.3, 0.8, 0.8)
		powerup:setStrokeColor(0)
		
		local function onLocalCollision(self, event)
			if (event.other == thumbCircle) then
				thumbCircleCollision = false
				thumbCircle:setFillColor(0.3, 0.8, 0.8, 0.5)
				invinciblePowerupCounter = invinciblePowerupCounter + 1
				
				function notInvincible()
					invinciblePowerupCounter = invinciblePowerupCounter - 1
					if alive and invinciblePowerupCounter == 0 then
						thumbCircleCollision = true
						thumbCircle:setFillColor(1, 1, 1)
					end
				end
				
				timer.performWithDelay(5000, notInvincible, 1)
				
				event.target:removeSelf()
				for i,v in pairs(powerups) do
					if v == event.target then
						table.remove(powerups, i)
					end
				end
			end
		end
		
		powerup.collision = onLocalCollision
		powerup:addEventListener("collision")
		table.insert(powerups, powerup)
	end
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

-----------------------------------------------------------------------------------------

return scene