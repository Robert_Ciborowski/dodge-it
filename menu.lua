-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

SCREEN_WIDTH = display.actualContentWidth
SCREEN_HEIGHT = display.actualContentHeight

--------------------------------------------

-- forward declarations and other locals
local playButton
local leaderboardButton
local noAdsButton
local otherButton
local scoreText

savaData = {}
jsonIO = require("json_io")

-- 'onRelease' event listener for playButton
local function onPlayBtnRelease()
	composer.removeScene("level1")
	
	-- go to level1.lua scene
	composer.gotoScene("level1", "fade", 500)
	
	return true	-- indicates successful touch
end

function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	-- display a background image
	local background = display.newImageRect("res/background.jpg", SCREEN_WIDTH, SCREEN_HEIGHT)
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newImageRect("res/logo.png", SCREEN_WIDTH * 0.7, SCREEN_WIDTH * 0.11136363636)
	titleLogo.x = display.contentCenterX
	titleLogo.y = SCREEN_HEIGHT * 0.1
	
	-- This is the high score text.
	scoreText = display.newText("", SCREEN_WIDTH / 2, SCREEN_HEIGHT * 0.3, "res/troika.otf", 42)
	
	if not jsonIO then
		print("Could not open json io!")
		scoreText.text = "High Score: 0"
	else
		local savaData2 = jsonIO.loadTable("save_data.json")
		if savaData2 == nil then
			print("Could not load save_data.json!")
			scoreText.text = "High Score: 0"
		else
			scoreText.text = "High Score: " .. savaData2["highScore"]
			saveData = savaData2
		end
	end
	
	-- create a widget button (which will loads level1.lua on release)
	playButton = widget.newButton{
		--label="Play",
		labelColor = {default = {255}, over = {128}},
		defaultFile="res/play_button.png",
		overFile="res/button-over.png",
		width = SCREEN_WIDTH / 2, height = SCREEN_HEIGHT / 8,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playButton.x = display.contentCenterX
	playButton.y = SCREEN_HEIGHT * 0.5
	
	leaderboardButton = widget.newButton{
		--label="Play",
		labelColor = {default = {255}, over = {128}},
		defaultFile="res/leaderboard_button.png",
		overFile="res/button-over.png",
		width = SCREEN_WIDTH / 5, height = SCREEN_WIDTH / 5,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	leaderboardButton.x = SCREEN_WIDTH / 4
	leaderboardButton.y = SCREEN_HEIGHT * 0.7
	
	noAdsButton = widget.newButton{
		--label="Play",
		labelColor = {default = {255}, over = {128}},
		defaultFile="res/no_ads_button.png",
		overFile="res/button-over.png",
		width = SCREEN_WIDTH / 5, height = SCREEN_WIDTH / 5,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	noAdsButton.x = SCREEN_WIDTH / 2
	noAdsButton.y = SCREEN_HEIGHT * 0.7
	
	otherButton = widget.newButton{
		--label="Play",
		labelColor = {default = {255}, over = {128}},
		defaultFile="res/other_button.png",
		overFile="res/button-over.png",
		width = SCREEN_WIDTH / 5, height = SCREEN_WIDTH / 5,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	otherButton.x = SCREEN_WIDTH * 3 / 4
	otherButton.y = SCREEN_HEIGHT * 0.7
	
	-- all display objects must be inserted into group
	sceneGroup:insert(background)
	sceneGroup:insert(titleLogo)
	sceneGroup:insert(scoreText)
	sceneGroup:insert(playButton)
	sceneGroup:insert(leaderboardButton)
	sceneGroup:insert(noAdsButton)
	sceneGroup:insert(otherButton)
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if playButton then
		playButton:removeSelf()	-- widgets must be manually removed
		playButton = nil
	end
	if leaderboardButton then
		leaderboardButton:removeSelf()	-- widgets must be manually removed
		leaderboardButton = nil
	end
	if noAdsButton then
		noAdsButton:removeSelf()	-- widgets must be manually removed
		noAdsButton = nil
	end
	if otherButton then
		otherButton:removeSelf()	-- widgets must be manually removed
		otherButton = nil
	end
	if scoreText then
		scoreText:removeSelf()	-- widgets must be manually removed
		scoreText = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene