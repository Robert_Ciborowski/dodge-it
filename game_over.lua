-----------------------------------------------------------------------------------------
--
-- game_over.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

SCREEN_WIDTH = display.actualContentWidth
SCREEN_HEIGHT = display.actualContentHeight

--------------------------------------------

-- forward declarations and other locals
local bigText
local scoreText
local restartButton
local menuButton

savaData = {}
jsonIO = require("json_io")

-- 'onRelease' event listener for restartButton
local function onRestartBtnRelease()
	composer.removeScene("level1")
	
	-- go to level1.lua scene
	composer.gotoScene("level1", "fade", 500)
	
	return true	-- indicates successful touch
end

local function onMenuBtnRelease()
	composer.removeScene("menu")
	
	-- go to level1.lua scene
	composer.gotoScene("menu", "fade", 500)
	
	return true	-- indicates successful touch
end

function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	-- display a background image
	local background = display.newImageRect("res/background.jpg", SCREEN_WIDTH, SCREEN_HEIGHT)
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- create/position logo/title image on upper-half of the screen
	local bigText = display.newText("", SCREEN_WIDTH / 2, SCREEN_HEIGHT * 0.3, "res/troika.otf", 62)
	
	if achievedHighScore then
		bigText.text = "High Score!"
	else
		bigText.text = "Game Over!"
	end
	
	-- This is the high score text.
	scoreText = display.newText("", SCREEN_WIDTH / 2, SCREEN_HEIGHT * 0.4, "res/troika.otf", 62)
	
	scoreText.text = "" .. math.floor(distanceTravelled + extraPoints)
	
	-- create a widget button (which will loads level1.lua on release)
	restartButton = widget.newButton{
		--label="Play",
		labelColor = {default = {255}, over = {128}},
		defaultFile="res/play_button.png",
		overFile="res/button-over.png",
		width = SCREEN_WIDTH / 2, height = SCREEN_HEIGHT / 8,
		onRelease = onRestartBtnRelease	-- event listener function
	}
	restartButton.x = display.contentCenterX
	restartButton.y = SCREEN_HEIGHT * 0.5
	
	menuButton = widget.newButton{
		--label="Play",
		labelColor = {default = {255}, over = {128}},
		defaultFile="res/leaderboard_button.png",
		overFile="res/button-over.png",
		width = SCREEN_WIDTH / 5, height = SCREEN_WIDTH / 5,
		onRelease = onMenuBtnRelease	-- event listener function
	}
	menuButton.x = SCREEN_WIDTH / 2
	menuButton.y = SCREEN_HEIGHT * 0.7
	
	-- all display objects must be inserted into group
	sceneGroup:insert(background)
	sceneGroup:insert(bigText)
	sceneGroup:insert(scoreText)
	sceneGroup:insert(restartButton)
	sceneGroup:insert(menuButton)
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if restartButton then
		restartButton:removeSelf()	-- widgets must be manually removed
		restartButton = nil
	end
	if menuButton then
		menuButton:removeSelf()	-- widgets must be manually removed
		menuButton = nil
	end
	if noAdsButton then
		noAdsButton:removeSelf()	-- widgets must be manually removed
		noAdsButton = nil
	end
	if otherButton then
		otherButton:removeSelf()	-- widgets must be manually removed
		otherButton = nil
	end
	if scoreText then
		scoreText:removeSelf()	-- widgets must be manually removed
		scoreText = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene